import './App.css';
import {useDispatch, useSelector} from "react-redux";
import {randomUser} from "./store";
import {createResource} from "./fetch";
import React, {useEffect, useState} from "react";

const Initialresource = createResource();


const Person = ({resource}) => {

    const person = resource.person.read()

    return(
        <div>
            {person.name.first}
        </div>
    )
}


export  class ErrorBoundary extends React.Component {
    state = { hasError: false, errror : null };

    static getDerivedStateFromError(error) {
        return { hasError: true , error};
    }

    componentDidCatch(error, errorInfo) {
        console.log(error, errorInfo)
    }

    render() {
        if (this.state.hasError) {
            return <h1>Something went wrong : {this.state.error.message}</h1>;
        }

        return this.props.children;
    }
}

function App() {

    const dispatch = useDispatch();
    const status = useSelector((state) => state.thunk.status);
    const value = useSelector((state) => state.thunk.value);
    const [resource, setResource] = useState(Initialresource)

    return (
        <div>

            <div>
                <h1>Redux</h1>
                <div
                    onClick={() => {
                            dispatch(randomUser())
                    }}
                >button
                </div>
                <div>
                    <h3>Status</h3>
                    <p>{status}</p>
                    <h3>Value</h3>
                    {
                        status === "fulfilled" && (
                            <p>{value.name.first}</p>
                        )
                    }
                </div>
            </div>

            <br/>
            <br/>
            <br/>
            <hr style={{
                width: "100vh",
                height: "1px",
                backgroundColor: "black"
            }
            }/>
            <br/>
            <br/>
            <br/>

            <div>
                <h1>Suspender</h1>
                <React.Suspense fallback={<h1>Loading person</h1>}>

                        <Person resource={resource} />
                </React.Suspense>
                <button onClick={() => {
                    setResource(createResource())
                }}>
                    refresh data
                </button>
            </div>

            <br/>
            <br/>
            <br/>
            <hr style={{
                width: "100vh",
                height: "1px",
                backgroundColor: "black"
            }
            }/>
            <br/>
            <br/>
            <br/>

            <h1>Mój wniosek</h1>
            <p>redux wersja jak zwraca rejected nie wywala błedu w suspender trzeba stosować Error boundry by nie wyjebało apki</p>
            <p>występowanie errora można wypłapać i zapisać w redux i stworzyć komponent który informuje użytkownika że coś się nie załatowało poprzez jakiś pop up czy coś i nie trzeba robić tego probsem tylko storem</p>
            <p> jeżeli użyjemy suspendera w głęboko usadzonym komponencie to jak chcemy przekazać go np na sama górę apki by wiedziano o tym że jest jakiś błąd ?</p>
            <p>czy suspeder ma inne użycie o który nie wiem ?</p>

        </div>
    )
}

export default App;
