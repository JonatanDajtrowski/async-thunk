import {configureStore, createAsyncThunk, createSlice} from "@reduxjs/toolkit";


export const randomUser = createAsyncThunk(
    'app/randomUser',
    async () => {

        return new Promise(
            resolve => {
                setTimeout(() => {
                    const res = fetch("https://randomuser.me/api").then(x => x.json()).then(x => x.results[0])
                    return resolve(res)
                }, 5000)
            }
        )
    }
)

export const appReducer = createSlice({
    name: "app",
    initialState: {
        value: {},
        status: "",
    },
    extraReducers: (builder) => {
        builder.addCase(randomUser.pending, (state, action) => {
            state.status = "pending"
        })
        builder.addCase(randomUser.rejected, (state, action) => {
            state.status = "rejected"
        })
        builder.addCase(randomUser.fulfilled, (state, action) => {
            console.log(action)
            state.value = action.payload
            state.status = "fulfilled"
        })

    },
})


export default configureStore({
    reducer: {
        thunk: appReducer.reducer
    }
})