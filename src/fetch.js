const wrapPromise = (promise) => {
    let status = "pending";
    let result = "";
    let suspender = promise.then(r => {
        status = "success";
        result = r
    },e => {
        status = "error"
        result = e
    })

    return {
        read(){
            if(status === "pending"){
                console.log(status)
                throw suspender
            }else if(status === "error"){
                console.log(status)
                throw result
                return status;
            }
            console.log(status)
            return result;
        }
    }
}


export const fetchPerson = () => {
    return new Promise(
        resolve => {
            setTimeout(() => {
                const res = fetch("https://randomuser.me/api").then(x => x.json()).then(x => x.results[0])
                return resolve(res)
            }, 5000)
        }
    )
}

export const createResource = () => {
    return{
        person: wrapPromise(fetchPerson()),
    }
}